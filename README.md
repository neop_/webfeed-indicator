# WebFeed Indicator
This is an extension for the [Firefox](https://www.mozilla.org/firefox/new/) web browser helping you discover [Web Feeds](https://en.wikipedia.org/wiki/Web_feed). Find the extension here: https://addons.mozilla.org/en-US/firefox/addon/webfeed-indicator/

## Description:
This extension displays an icon in your address bar if it detects RSS, Atom, or JSON Web Feeds on the active website. Click on it to open a Popup showing the feed URLs and a Button to copy the corresponding URL to your clipboard with one click.

You can paste the URL into your favorite feed aggregator software to subscribe to it.
When the extension is installed and given permission, visit https://blog.mozilla.org to try it out. The icon should appear in your address bar.

Compared to other feed extensions, this one puts an emphasis on a limited feature set and quality.

Features:
- Good theme integration
- Open Source
- Works completely on your local computer
- Simple, limited feature creep

Permissions:
To extract the feed URLs websites advertise from their HTML head tag, this extension unfortunately requires being able to read the content of the websites you visit. Feel free to verify in the <a href="https://gitlab.com/neop_/webfeed-indicator">source code</a> that this is only used for a simple local HTML query.