let feed_selector = "link[rel=alternate][href][type='application/rss+xml'],link[rel=alternate][href][type='application/feed+json'],link[rel=alternate][href][type='application/atom+xml']";

let feeds = Array.from(document.head.querySelectorAll(feed_selector))
	.flatMap(link => {
		try {
			let url = new URL(link.href);
			if (/* I only want to subscribe https feeds */url.protocol !== "https:") {
				return [];
			}
			return [url.href];
		} catch (error) {
			// ignore malformed urls
			return [];
		}
	});

browser.runtime.onMessage.addListener(async (_message) => {
	return feeds;
});

// show page action if there are feeds
if (feeds.length > 0) {
	browser.runtime.sendMessage("pls show page action");
}