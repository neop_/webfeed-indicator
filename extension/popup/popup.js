
browser.tabs.query({ active: true, currentWindow: true })
	.then(async (active_tabs) => {
		const feeds = await browser.tabs.sendMessage(
			active_tabs[0].id,
			"feed me"
		);
		feeds.forEach(href => {
			const template = document.getElementById("feed_template");
			const instance = template.content.cloneNode(true);
			const a = instance.querySelector("a");
			a.innerText = href;
			a.href = href;
			instance.querySelector("button").addEventListener("click", () => {
				navigator.clipboard.writeText(href);
			});
			document.body.appendChild(instance);
		});
	});


browser.theme.getCurrent().then((theme) => {
	const style = document.createElement("style");
	style.innerText = `
		body {
			background-color: ${theme.colors?.popup ?? "inherit"};
		}
		
		a {
			color: ${theme.colors?.popup_text ?? "inherit"};
		}

		button {
			stroke: ${theme.colors?.popup_text ?? "rgb(34, 36, 38)"};
			background-color: ${theme.colors?.button ?? "hsla(0, 0%, 0%, 0.3)"};
			border-radius: 3px;
		}
		
		button:hover {
			background-color: ${theme.colors?.button_hover ?? "hsla(0, 0%, 100%, 0.3)"};
		}

		button:active {
			background-color: ${theme.colors?.button_active ?? "hsla(0, 0%, 100%, 0.4)"};
		}

	`;
	document.head.appendChild(style);
});