


function make_icon_svg_string(color) {
	return '<svg width="19" height="19" version="1.1" viewBox="0 0 5.0271 5.0271" xmlns="http://www.w3.org/2000/svg"><circle cx=".65944" cy="4.3676" r=".65944"  style="fill:#000"/><circle cx="-1.7881e-7" cy="5.0271" r="4.245" style="fill:none;stroke-linecap:square;stroke-width:.79375;stroke:#000"/><circle cx="-1.1102e-16" cy="5.0271" r="2.7311" style="fill:none;stroke-linecap:square;stroke-width:.79375;stroke:#000"/></svg>'.replaceAll("#000", color);
}

function make_icon_data_uri(color) {
	return `data:image/svg+xml;base64,${btoa(make_icon_svg_string(color))}`;
}

browser.runtime.onMessage.addListener(async (message, sender) => {
	const theme = await browser.theme.getCurrent();
	await browser.pageAction.setIcon({ path: make_icon_data_uri(theme.colors?.toolbar_field_text_focus ?? theme.colors?.toolbar_field_text ?? "rgb(209, 209, 209)"), tabId: sender.tab.id });
	browser.pageAction.show(sender.tab.id);
});
